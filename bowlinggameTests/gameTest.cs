﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit;
using NUnit.Framework;
using keilausjuttu;

namespace bowlinggameTests
{ [TestFixture]
    public class gameTest
    {
        private Bowlinggame game;
        [SetUp]
        public void SetUp()
        {
            game = new Bowlinggame();
        }

        [Test]
        public void CanRollGutterGame()
        {
            RollMany(0, 20);
            Assert.AreEqual(0, game.Score);
        }

        [Test]
        public void CanRollAllOnes()
        {
            RollMany(1, 20);
            Assert.AreEqual(20, game.Score);
        }

        private void RollMany(int pins, int rolls)
        {
            for (var i = 0; i < rolls; i++)
                game.Roll(pins);
        }
    }
}
